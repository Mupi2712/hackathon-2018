import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Paho} from 'ng2-mqtt/mqttws31';
import {AngularFireDatabase} from 'angularfire2/database'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private _client_temp;
  private _client_hum;
  public fh ;
  public ft;
  public time;
  public room = "Keller";
  warning="";



  public constructor(public fdb: AngularFireDatabase) {
   this.getDataFromFirebase()
    
  }

  
  check(){
    if (this.fh>=60){
      if (this.ft>=0){
        this.warning ="Schimmelgefahr!! Bitte lüften! Danke fürs Zuhören :)"
      }
    }
  }
 getDataFromFirebase(){
    this.fdb.list('/Humi').valueChanges().subscribe(
      data=>{
        let index = data.length
        console.log("Humi: "+JSON.stringify(data));
        this.fh=JSON.stringify(data[index-1])
		this.check()
		let a = new Date();
		this.time= a.getHours()+":"+a.getMinutes()+":"+a.getSeconds();
      }
    )
   this.fdb.list('/Temp').valueChanges().subscribe(
     datas=>{
       let index= datas.length
       console.log("Temp:"+JSON.stringify(datas));
       this.ft=JSON.stringify(datas[index-1])
	   this.check()
	   let a = new Date();
	   this.time= a.getHours()+":"+a.getMinutes()+":"+a.getSeconds();
     }
   )
 }
}
