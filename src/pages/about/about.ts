import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Paho} from 'ng2-mqtt/mqttws31';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
	  public temp ;
  public h;
  public t;
  public r;

  constructor(public navCtrl: NavController) {
	this._client_temp = new Paho.MQTT.Client('172.16.0.1', 1884, "temp")
    this._client_hum = new Paho.MQTT.Client('172.16.0.1', 1884, "humi");

    this._client_temp.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost.');
    };
    this._client_hum.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost.');
    };

    this._client_temp.onMessageArrived = (message: Paho.MQTT.Message) => {
      //if (message.destinationName=="house/keller/temp"){
      console.log("Temperatur: "+message.payloadString+" in "+message.destinationName)
      this.temp=+message.payloadString;
      let a = new Date();
      this.t= a.getHours()+":"+a.getMinutes()+":"+a.getSeconds();
      this.check()
    };
    this._client_hum.onMessageArrived = (message: Paho.MQTT.Message) => {
      //if (message.destinationName=="house/keller/hum"){
      console.log("Luftfeuchtigkeit: "+message.payloadString+" in "+message.destinationName)
      this.h=+message.payloadString;
      let a = new Date();
      this.t= a.getHours()+":"+a.getMinutes()+":"+a.getSeconds();
      this.check()
    };

    this._client_temp.connect({onSuccess: this.onConnected_temp.bind(this)});
    this._client_hum.connect({onSuccess: this.onConnected_hum.bind(this)});
  }
	private onConnected_temp(): void {
    this._client_temp.subscribe("haus/schimmel/temp")
    this.r="Keller";
    console.log('Connected to broker.');
  }
  private onConnected_hum(): void {
    this._client_hum.subscribe("haus/schimmel/humi");
    this.r="Keller";
    console.log('Connected to broker.');
  }
    check(){
    if (this.h>=60){
      if (this.temp>=0){
        this.warning ="Schimmelgefahr!! Bitte lüften!"
      }
    }
  }
}
