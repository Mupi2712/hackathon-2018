#include <ArduinoJson.h>
#include <Firebase.h>
#include <FirebaseArduino.h>
#include <FirebaseCloudMessaging.h>
#include <FirebaseError.h>
#include <FirebaseHttpClient.h>
#include <FirebaseObject.h>
#include <SimpleDHT.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>


WiFiClient espClient;
PubSubClient client(espClient);

int dht_pin = 4;
SimpleDHT11 dht11(dht_pin);

#define FIREBASE_HOST "pithackathon.firebaseio.com"
#define FIREBASE_AUTH "Temp"

void setup()
{
  Serial.begin(115200);
  Serial.println();

  WiFi.begin("Forum", "Hack2018");

  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());

  client.setServer("172.16.0.1", 1883);
  client.setCallback(callback);

  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

void loop() 
{
  byte temperature;
  byte humidity;
  int t;
  int h;
  String tString, hString;
  
  int data;

  int err = SimpleDHTErrSuccess;
  if ((err = dht11.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) 
  {
    Serial.print("Read DHT11 failed, err="); Serial.println(err);
    delay(2000);
    return;
  }
  
  Serial.print("Sample OK: ");
  Serial.print((int)temperature); Serial.print(" *C, "); 
  Serial.print((int)humidity); Serial.println(" H");
  
  if (!client.connected()) 
  {
    reconnect();
  }
  client.loop();

  client.publish("haus/schimmel/temp", String(temperature).c_str(), true);
  client.publish("haus/schimmel/humi", String(humidity).c_str(), true);

  Serial.print("Temp: ");
  Serial.println(temperature);

  Serial.print("Humi: ");
  Serial.println(humidity);

  if (Firebase.failed()) 
  {
      Serial.print("setting value failed:");
      Serial.println(Firebase.error());  
      return;
  }
  delay(1000);

  Firebase.remove("Temp");
  delay(1000);

 // set string value
  Firebase.setString("Temp", String(temperature).c_str());
  // handle error
  if (Firebase.failed()) {
      Serial.print("setting /message failed:");
      Serial.println(Firebase.error());  
      return;
  }
  delay(2000);
}

void reconnect() 
{
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) 
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is acive low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }
}
